#!/usr/bin/env python
from __future__ import unicode_literals
import socket
import os
import sys
root = os.path.dirname(os.path.abspath('__file__'))
sys.path.insert(0, os.path.join(root, 'md_ext'))

DEPLOY = os.environ.get('DEPLOY', '0').lower() in ('1', 'yes', 'y', 'true', 't')

# Site settings
AUTHOR = u'Dennis Kaarsemaker'
SITENAME = u'Git Cookbook'
SITETITLE = "Git cookbook"
SITEURL = 'https://git.seveas.net' if DEPLOY else 'http://localhost:8000'
TIMEZONE = 'Europe/Amsterdam'
DEFAULT_LANG = u'en'
LOCALE = 'en_US.UTF-8'
THEME = os.path.join(root, 'themes', 'Flex')
CUSTOM_CSS = '/static/css/cookbook.css'
SITELOGO = '/static/img/git.png'

# Content
PATH = 'content'
STATIC_PATHS = ['static']
SUMMARY_MAX_LENGTH = 100
DISPLAY_PAGES_ON_MENU = False
DEFAULT_PAGINATION = 30
DEFAULT_ORPHANS = 10
PAGE_URL = PAGE_SAVE_AS = '{slug}.html'

# Feeds
FEED_DOMAIN = SITEURL
FEED_ALL_ATOM = 'feeds/atom.xml'
FEED_ALL_RSS = 'feeds/rss.xml'
CATEGORY_FEED_ATOM = None
TRANSLATION_FEED_ATOM = None
AUTHOR_FEED_ATOM = None
AUTHOR_FEED_RSS = None
FEED_USE_SUMMARY = True

# Links
SOCIAL = (
    ('git', 'https://gitlab.com/seveas/git_cookbook'),
    ('twitter', 'https://twitter.com/seveas'),
    ('rss', '/feeds/atom.xml'),
    ('rss', '/feeds/rss.xml'),
)
LINKS = (
            ('Categories', '/categories.html'),
            ('Tags', '/tags.html'),
            ('Manpages', '/manpages/'),
            ('About', '/about.html'),
        )

# Plugins and extensions we use
DISQUS_SITENAME = 'gitcookbook' if DEPLOY else None
MD_EXTENSIONS = [
    'markdown.extensions.extra',
    'gcb.codehilite(css_class=highlight)',
    'gcb.admonition',
    'gcb.graphs',
]
PLUGIN_PATHS = ['/home/dennis/code/pelican-plugins']
PLUGINS = [
    'feed_summary',
    'tag_cloud',
]
