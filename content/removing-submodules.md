Title: Getting rid of submodules
Category: Repository maintenance
Date: 2015-11-07
Tags: submodules
Summary: Getting rid of submodules is as cumbersome as any submodule operation. So let's have no more of it and delete some submodules.

I'm not a fan of submodules. They do have their place, and they can be used in
a good way. But they're cumbersome to use and they're too often used as a poor
substitute for properly managing dependencies.

So here's how to delete a submodule from your repository that you've added for
the wrong reason, or want to get rid of for any other reason. 

* Delete the submodule from .gitmodules. If there are no more submodules left,
  remove the file completely.
* `git add .gitmodules` to tell git about the change
* `rm -rf path/to/submodule` to get rid of the files
* `git rm -f --cached path/to/submodule` to tell git to get rid of the files.
  Having to do this in two steps is one of the things that is cumbersome about
  submodules.
* `git commit`

Once you've committed that change, you will need to inspect .git/config and
.git/modules for more leftovers that need removing.
