define(['monkey', 'd3', 'historyview'], function(monkey, d3, HistoryView) {

    function Graph(config, container) {
        this.config = JSON.stringify(config); /* Saving as string to allow easy deep copying */
        this.container = d3.select(container);
        this.info_window = this.container.append('div');
        this.info_window.classed('info-window', true).classed('hidden', true);
        this.info_window[0][0].appendChild(d3.select('#graph-help')[0][0].cloneNode(true));

        this.historyView = null;
        this.remoteViews = [];
        var btn = this.container.append('span');
        btn.attr('onclick', 'all_graphs.' + config.name + '.reset()')
           .classed('fa', true)
           .classed('fa-undo', true)
           .classed('reset-button', true);
        btn = this.container.append('span');
        btn.attr('onclick', 'all_graphs.' + config.name + '.info()')
           .classed('fa', true)
           .classed('fa-info-circle', true)
           .classed('info-button', true);
    }

    Graph.prototype = {
        reset: function() {
            this.info_window.classed('hidden', true);
            this.container.style('display', 'block');
            if (this.historyView) {
                this.historyView.remotes = {};
                this.historyView.destroy();
            }
            for (var i=0; i<this.remoteViews.length; i++)
                this.remoteViews[i].destroy();
            this.remoteViews = [];
            var config = JSON.parse(this.config);
            this.historyView = new HistoryView(config);
            this.historyView.render(this.container);
            for(var i=0; i<config.remotes.length; i++){
                config.remotes[i].pos = i;
                this.remoteViews[i] = new HistoryView(config.remotes[i]);
                for (var j=0; j<this.remoteViews[i].commitData.length; j++) {
                    var rc = this.remoteViews[i].commitData[j];
                    var lc = this.historyView.getCommit(rc.idx);
                    if(lc)
                        rc.id = lc.id;
                }
                this.remoteViews[i].render(this.container);
                this.historyView.remotes[this.remoteViews[i].remoteName] = this.remoteViews[i];
            }
            return this;
        },
        info: function() {
            this.info_window.classed('hidden', !this.info_window.classed('hidden'));
        }
    }

    return Graph;
});
