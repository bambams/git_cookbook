Title: Always using the latest and greatest git
Category: Effective git usage
Date: 2015-11-04
Tags: next, install
Summary: Help testing Git development by using packages that integrate with
    Ubuntu, Debian CentOS and Fedora.

Git is under very active development, and both bugfixes and awesome new
features are regularly added to the development version of git in a branch
called 'next'. The git developers are also remarkable good at not breaking this
development branch. Many people actually use this branch for their day-to-day
git usage.

A downside of using this version of git is that you'll have to manually compile
and install it, which is not only repetitive work, it  also doesn't play nice
with packaging systems like apt or yum. So why not let someone else solve that
problem for you?

I have set up a [continuous integration](http://ci.kaarsemaker.net/git/) system
that builds new deb and rpm source packages every day if the git maintainer has
pushed new commits to git.git. These source packages are uploaded to [Launchpad](https://launchpad.net/~dennis/+archive/ubuntu/git-next/+packages)
and
[openbuildservice](https://build.opensuse.org/package/show/home:seveas:git-next/git)
to create packages for Debian, Ubuntu, Fedora and CentOS. It now becomes really
easy to use and regularly upgrade git on your system.

Note that these packages explicitly do *not* use the local patches that Debian
and Fedora add to git. They give you a git as vanilla as possible.

Ubuntu
------
    :::console
    $ sudo-i
    # add-apt-repository ppa:dennis/git-next
    # apt-get update
    # apt-get -y install git

CentOS 7
--------
    :::console
    $ sudo-i
    # cd /etc/yum.repos.d
    # wget http://download.opensuse.org/repositories/home:/seveas:/git-next/CentOS_7/home:seveas:git-next.repo
    # yum -y install git

CentOS 6 is also supported, just change 7 to 6 in the wget command

Debian 8.0
----------
    :::console
    $ sudo-i
    # echo 'deb http://download.opensuse.org/repositories/home:/seveas:/git-next/Debian_8.0/ /' >> /etc/apt/sources.list.d/git-next.list
    # apt-get update
    # apt-get install git

Debian 7.0 is also supported, again just change the url.

Fedora 23
---------
    :::console
    $ sudo-i
    # cd /etc/yum.repos.d
    # wget http://download.opensuse.org/repositories/home:/seveas:/git-next/Fedora_23/
    # yum -y install git

Fedora 20, 21 and 22 are also supported, just change the download link to match.

Automatic upgrades
------------------
To automatically upgrade git, just add a crontab entry that runs the yum or apt-get install command above every day.
