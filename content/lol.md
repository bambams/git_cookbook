Title: Showing all branches and their relationships
Category: Digging through history
Date: 2015-11-01
Tags: log, branch
Summary: You don't need a graphical interface to see all your branches and their relationships.

To get a more "graphical" view of your branches, similar to what `gitk` and
`git gui` provide, simply provide the following arguments to `git log`:


* `--oneline` to summarize each commit in one line
* `--graph` to draw lines between connected commits 
* `--decorate` to show branch/tag names
* `--all` to process all branches and tags

The output looks somewhat like this:

    :::console
    $ git log --oneline --graph --decorate --all
    * [33m928e747[m[33m ([m[1;36mHEAD[m[33m -> [m[1;32mmaster[m[33m, [m[1;31morigin/master[m[33m, [m[1;31morigin/HEAD[m[33m)[m set_ers_direct_connect: password wasn't passed correctly
    [31m|[m * [33m66b4165[m[33m ([m[1;31morigin/debian[m[33m, [m[1;32mdebian[m[33m)[m New debian package
    [31m|[m *   [33m24deb65[m Merge tag '3.3' into debian
    [31m|[m [33m|[m[31m\[m  
    [31m|[m [33m|[m[31m/[m  
    [31m|[m[31m/[m[33m|[m   
    [31m|[m * [33mc58e62c[m New debian package
    [31m|[m *   [33m7d3a9ce[m Merge tag '3.2' into debian
    [31m|[m [35m|[m[36m\[m  
    [31m|[m * [36m|[m [33ma70c56d[m New debian package
    [31m|[m * [36m|[m   [33m6509932[m Merge tag '3.1' into debian
    [31m|[m [1;31m|[m[1;32m\[m [36m\[m  
    [31m|[m * [1;32m|[m [36m|[m [33mff1214f[m New debian package
    [31m|[m * [1;32m|[m [36m|[m   [33m19e8dc2[m Merge tag '3.0' into debian
    [31m|[m [1;33m|[m[1;34m\[m [1;32m\[m [36m\[m  
    [31m|[m * [1;34m|[m [1;32m|[m [36m|[m [33ma161e09[m[33m ([m[1;31mduncanwebb/debian[m[33m)[m New debian package
    [31m|[m * [1;34m|[m [1;32m|[m [36m|[m   [33m6428b02[m Merge tag '2.13.1' into debian
    [31m|[m [1;35m|[m[1;36m\[m [1;34m\[m [1;32m\[m [36m\[m  
    [31m|[m * [1;36m|[m [1;34m|[m [1;32m|[m [36m|[m [33m6ae54b1[m New debian package
    [31m|[m * [1;36m|[m [1;34m|[m [1;32m|[m [36m|[m   [33md22ee26[m Merge tag '2.13' into debian
