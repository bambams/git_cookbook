The Git cookbook
----------------
This is the content and code behind [the Git cookbook](http://git.seveas.net). 

The Git cookbook is a collection of recipes for using git and pages with
background information. It is written and maintained by Dennis Kaarsemaker,
based on many years of using, supporting  and troubleshooting git in the
workplace and on IRC, maintaining git servers for tens to hundreds of
developers and building tools with and around git. The cookbook aims to provide
solutions for common pitfalls, insight into git's interface and inner workings
and tips & tricks for recovering from problems caused by git or using git
wrong.

If you have a question, comment or suggestion about a specific article, please
file an issue or leave a comment on the cookbook itself. Suggestions for
subjects of new articles are also welcome, the cookbook needs to grow!
